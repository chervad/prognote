FROM chervad/msvc2017_qt5131

WORKDIR C:\\TEMP\\prognote
COPY qprognote C:\\TEMP\\prognote\\qprognote
COPY build.cmd C:\\TEMP\\prognote\\build.cmd
COPY deploy.bat C:\\TEMP\\prognote\\deploy.bat
COPY 7z.exe C:\\TEMP\\prognote\\7z.exe
COPY 7z.dll C:\\TEMP\\prognote\\7z.dll
COPY upx.exe C:\\TEMP\\prognote\\upx.exe

RUN setx PATH "%PATH%;%QTDIR%\\bin"

RUN build.cmd

RUN deploy.bat


