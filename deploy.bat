set QTDIR=C:\Qt\Qt5.13.1\5.13.1\msvc2017
set APP_ROOT=%CD%
set DEPLOY_DIR=%APP_ROOT%\deploy
set VCINSTALLDIR=C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC
set PATH=%QTDIR%\bin;%PATH%

@rem rmdir %APP_ROOT%\iconengines /q /s
@rem rmdir %APP_ROOT%\imageformats /q /s
@rem rmdir %APP_ROOT%\platforms /q /s
@rem rmdir %APP_ROOT%\translations /q /s
@rem rmdir %DEPLOY_DIR% /q /s

mkdir %DEPLOY_DIR%

windeployqt.exe --libdir %DEPLOY_DIR% --release --force --compiler-runtime %APP_ROOT%\_build\Release\qprognote.exe

del %DEPLOY_DIR%\D3Dcompiler_47.dll
del %DEPLOY_DIR%\libEGL.dll
del %DEPLOY_DIR%\libGLESV2.dll
del %DEPLOY_DIR%\opengl32sw.dll
del %DEPLOY_DIR%\Qt5Svg.dll
del %DEPLOY_DIR%\vcredist_x86.exe

mkdir %DEPLOY_DIR%\platforms
mkdir %DEPLOY_DIR%\sqldrivers

copy %APP_ROOT%\_build\Release\platforms\qwindows.dll %DEPLOY_DIR%\platforms
copy %APP_ROOT%\_build\Release\sqldrivers\qsqlite.dll %DEPLOY_DIR%\sqldrivers
copy %APP_ROOT%\_build\Release\qprognote.exe %DEPLOY_DIR%
copy %APP_ROOT%\qprognote\phill_new.dat %DEPLOY_DIR%

@rem upx.exe -5 %DEPLOY_DIR%\Qt5Gui.dll
@rem upx.exe -5 %DEPLOY_DIR%\Qt5Widgets.dll
@rem upx.exe -5 %DEPLOY_DIR%\Qt5core.dll
@rem upx.exe -1 %DEPLOY_DIR%\platforms\qwindows.dll
@rem upx.exe -5 %DEPLOY_DIR%\sqldrivers\qsqlite.dll
@rem upx.exe -5 %DEPLOY_DIR%\qprognote.exe

upx.exe %DEPLOY_DIR%\Qt5Widgets.dll %DEPLOY_DIR%\Qt5Gui.dll

@rem https://www.dmosk.ru/miniinstruktions.php?mini=7zip-cmd#options-keys
7z.exe a -tzip -mx5 -r0 .\qprognote.zip .\deploy\*.*

@rem pause