#ifndef DATALISTMODEL_H
#define DATALISTMODEL_H

#include <QAbstractListModel>

class dataListModel : public QAbstractListModel
{
	Q_OBJECT
private:
        QList <QPair<int, QString> > m_lstData;
public:
        dataListModel(QObject *parent = NULL);
    virtual ~dataListModel() {}
public:
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
        void setList(QList <QPair<int, QString> > &lstData);
    int getId(const QModelIndex &index);
};

#endif // DATALISTMODEL_H
