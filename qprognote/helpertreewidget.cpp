#include "helpertreewidget.h"
#include "utils.h"
#include "wndmain.h"

class Utils;

HelperTreeWidget::HelperTreeWidget(QWidget *parent /* = NULL */)
        : QTreeWidget(parent)
{
    db = NULL;
}

void HelperTreeWidget::focusOutEvent(QFocusEvent *event) {
	if (selectedItems().count() > 0) {
		emit signalFocus(true);
	}
	else {
		emit signalFocus(false);
	}
}

void HelperTreeWidget::setDB(QSqlDatabase *_db) {
    if (_db) db = _db;
}

void HelperTreeWidget::deleteTreeRecursive(int id) {
	SQLEXEC_DATA sqlRes0 = Utils::sqlExecute(db, "SELECT id FROM tree_tbl WHERE parent = %d", id);
	SQLEXEC_TABLE::iterator resIterator0 = sqlRes0.table.begin();

	for (; resIterator0 != sqlRes0.table.end(); ++resIterator0) {
		this->deleteTreeRecursive((*resIterator0)["id"].toInt());
	}

	Utils::sqlExecute(db, "DELETE FROM data_tbl WHERE tree_id = %d", id);
	Utils::sqlExecute(db, "DELETE FROM tree_tbl WHERE id = %d", id);
}

void HelperTreeWidget::addTreeRecursive(QTreeWidgetItem *ptwgParentItem, int parent) {
    if (!ptwgParentItem || !db) return;

    HelperTreeWidgetItem *ptwgItem = NULL, *ptwgSubItem = NULL;
    QSqlQuery query(*db), query_data(*db);
    QSqlRecord rec, rec_data;

    SQLEXEC_DATA sqlRes0 = Utils::sqlExecute(db, "SELECT id, title FROM tree_tbl WHERE parent = %d", parent);
    SQLEXEC_TABLE::iterator resIterator0 = sqlRes0.table.begin();

    QString sql;
    for (; resIterator0 != sqlRes0.table.end(); ++resIterator0) {
        ptwgItem = new HelperTreeWidgetItem(ptwgParentItem);
        ptwgItem->setText(0, (*resIterator0)["title"]);
        ptwgItem->setTag((*resIterator0)["id"].toInt());

        addTreeRecursive(ptwgItem, ptwgItem->tag());
        /*добавляем элементы из data_tbl*/
        SQLEXEC_DATA sqlRes1 = Utils::sqlExecute(db, "SELECT id, title, data FROM data_tbl WHERE tree_id = %d", ptwgItem->tag());
        SQLEXEC_TABLE::iterator resIterator1 = sqlRes1.table.begin();

        for (; resIterator1 != sqlRes1.table.end(); ++resIterator1) {
            ptwgSubItem = new HelperTreeWidgetItemData(ptwgItem);
            ptwgSubItem->setText(0, (*resIterator1)["title"]);
            ptwgSubItem->setTag((*resIterator1)["id"].toInt());
        }
    }
}

void HelperTreeWidget::refreshLeftTree() {
    HelperTreeWidgetItem *ptwgItem = NULL, *ptwgSubItem = NULL;
    if (!db) return;
	
	clear();
    SQLEXEC_DATA sqlRes0 = Utils::sqlExecute(db, "SELECT id, title FROM tree_tbl WHERE parent = 0");
    SQLEXEC_TABLE::iterator resIterator0 = sqlRes0.table.begin();

    for (; resIterator0 != sqlRes0.table.end(); ++resIterator0) {
        ptwgItem = new HelperTreeWidgetItem(this);
        ptwgItem->setText(0, (*resIterator0)["title"]);
        ptwgItem->setTag((*resIterator0)["id"].toInt());
        addTreeRecursive(ptwgItem, ptwgItem->tag());

        SQLEXEC_DATA sqlRes1 = Utils::sqlExecute(db, "SELECT id, title, data FROM data_tbl WHERE tree_id = %d", ptwgItem->tag());
        SQLEXEC_TABLE::iterator resIterator1 = sqlRes1.table.begin();

        for (; resIterator1 != sqlRes1.table.end(); ++resIterator1) {
            ptwgSubItem = new HelperTreeWidgetItemData(ptwgItem);
            ptwgSubItem->setText(0, (*resIterator1)["title"]);
            ptwgSubItem->setTag((*resIterator1)["id"].toInt());
        }
    }
}

bool HelperTreeWidget::haveSubTree(HelperTreeWidgetItem *ptwgItem) {
	bool res = false;
	SQLEXEC_DATA sqlRes0, sqlRes1;

	sqlRes0 = Utils::sqlExecute(db, "SELECT count() as count FROM tree_tbl WHERE parent = %d", ptwgItem->tag());
	sqlRes1 = Utils::sqlExecute(db, "SELECT count() as count FROM data_tbl WHERE tree_id = %d", ptwgItem->tag());
	if (sqlRes0.table.at(0)["count"].toInt() > 0 || sqlRes1.table.at(0)["count"].toInt() > 0) {
		res = true;
	}

	return res;
}

bool HelperTreeWidget::delSubTreePart(HelperTreeWidgetItem *ptwgItem) {
	bool res = false;
	//удалить подраздел знания
	if (ptwgItem->type() == HelperTreeWidgetItem::DATA_TYPE) {
		delDataPart(static_cast<HelperTreeWidgetItemData *>(ptwgItem));
	}
	//удалить раздел 
	else {
		// to-do добавить проверку на наличие детей в данном разделе
		Utils::sqlExecute(db, "DELETE FROM tree_tbl WHERE id = %d", ptwgItem->tag());
		res = !Utils::isError();
		delete ptwgItem;
	}
	return res;
}

bool HelperTreeWidget::delDataPart(HelperTreeWidgetItem *ptwgItemData) {
	bool res = false;
	//удалить подраздел знания
	if (ptwgItemData->type() == HelperTreeWidgetItem::DATA_TYPE) {
		Utils::sqlExecute(db, "DELETE FROM data_tbl WHERE id = %d", ptwgItemData->tag());
		res = !Utils::isError();
	}
	delete ptwgItemData;
	return res;
}

bool HelperTreeWidget::dropMimeData(QTreeWidgetItem *parent, int index, const QMimeData *data, Qt::DropAction action)  
{
	HelperTreeWidgetItem *child = dynamic_cast<HelperTreeWidgetItem *>(currentItem());
	HelperTreeWidgetItem *parent_ch = dynamic_cast<HelperTreeWidgetItem *>(parent);

	if (child->type() == HelperTreeWidgetItem::DATA_TYPE && parent_ch->type() == HelperTreeWidgetItem::TREE_TYPE) {
			Utils::sqlExecute(db, "UPDATE data_tbl SET tree_id = %d WHERE id = %d", parent_ch->tag(), child->tag());
			refreshLeftTree();
			expandAll();
	}
	else if (child->type() == HelperTreeWidgetItem::TREE_TYPE && parent_ch->type() == HelperTreeWidgetItem::TREE_TYPE &&
			 parent_ch->tag() != child->tag()) {
			bool isSubTree = false; //проверяем, не является-ли parent_ch поддеревом child для исключения закольцованности
			SQLEXEC_DATA res = Utils::sqlExecute(db, "SELECT parent FROM tree_tbl WHERE id = %d", parent_ch->tag());
			int cur_parrent = res.table.at(0)["parent"].toInt();
			if (cur_parrent > 0) {
				do {
					if (cur_parrent == child->tag()) {
						isSubTree = true;
						break;
					}
					res = Utils::sqlExecute(db, "SELECT parent FROM tree_tbl WHERE id = %d", cur_parrent);
					cur_parrent = res.table.at(0)["parent"].toInt();
				} while (cur_parrent > 0);
			}
			if (!isSubTree) {
				Utils::sqlExecute(db, "UPDATE tree_tbl SET parent = %d WHERE id = %d", parent_ch->tag(), child->tag());
				refreshLeftTree();
				expandAll();
			}
			else {
				Utils::showError(QString("Невозможно перенести родительский раздел в дочерний подраздел!"), this);
				return false;
			}
	}
	return true;
}