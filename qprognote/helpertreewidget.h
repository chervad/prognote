#ifndef HELPERTREEWIDGET_H
#define HELPERTREEWIDGET_H

#include <QTreeWidget>
#include <QtGui>
#include <QtSql>
#include "utils.h"
#include "helpertreewidgetitem.h"
#include "helpertreewidgetitemdata.h"

class HelperTreeWidget : public QTreeWidget
{
    Q_OBJECT
public:
    HelperTreeWidget(QWidget *parent = NULL);
	~HelperTreeWidget() {}
    void setDB(QSqlDatabase *_db = NULL);
	QSqlDatabase *getDB() { return db; }
    void addTreeRecursive(QTreeWidgetItem *ptwgParentItem, int parent);
	void deleteTreeRecursive(int id);
    void refreshLeftTree();
	bool haveSubTree(HelperTreeWidgetItem *ptwgItem);
	bool delSubTreePart(HelperTreeWidgetItem *ptwgItem);
	bool delDataPart(HelperTreeWidgetItem *ptwgItemData);
protected:
	virtual void focusOutEvent(QFocusEvent *event);
	virtual bool dropMimeData (QTreeWidgetItem *parent, int index, const QMimeData *data, Qt::DropAction action);
private:
    QSqlDatabase *db;
signals:
	void signalFocus(bool selected_item);
};

#endif // HELPERTREEWIDGET_H
