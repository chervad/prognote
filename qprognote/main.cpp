﻿#include <QtWidgets/QApplication>
#include <QMessageBox>
#include <QLocale>
#include <QFile>
#include <QTextStream>
#include <QCommandLineParser>
#include <QDebug>

#include <stdio.h>
#include <stdlib.h>
#include <memory>

#include <QApplication>
#include "wndmain.h"

std::shared_ptr<QFile> logFile;

#define ORGANIZATION_NAME "Chervad"
#define ORGANIZATION_DOMAIN "www.chervad.ru"
#define APPLICATION_NAME "Блокнот"
#define APPLICATION_VERSION "1.0"

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	QTextStream out(logFile.get());
	out << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ");
	switch (type)
	{
	case QtInfoMsg:     out << "[INFO ]"; break;
	case QtDebugMsg:    out << "[DEBUG] "; break;
	case QtWarningMsg:  out << "[WARN ]"; break;
	case QtCriticalMsg: out << "[CRIT ]"; break;
	case QtFatalMsg:    out << "[FATAL] "; break;
	}
	out << msg << endl;
	out.flush();
}

int main(int argc, char *argv[])
{
	QLocale::setDefault(QLocale(QLocale::Russian, QLocale::RussianFederation));

    QApplication a(argc, argv);

	QCoreApplication::setOrganizationName(ORGANIZATION_NAME);
	QCoreApplication::setOrganizationDomain(ORGANIZATION_DOMAIN);
	QCoreApplication::setApplicationName(APPLICATION_NAME);
	QCoreApplication::setApplicationVersion(APPLICATION_VERSION);

    wndMain w;
    w.show();
    w.resize(800, 600);
    return a.exec();
}
