#include "wgtIndexDock.h"
#include "Utils.h"
#include "wndmain.h"

#include <QLineEdit>

wgtIndexDock::wgtIndexDock(const QString &title, QWidget *parent, Qt::WindowFlags flags)
    : QDockWidget(title, parent)
        , modelList()
        , p_Wgt(this)
{
	db = dynamic_cast<wndMain *>(parent)->getDBPointer();
	setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);

        p_vBox = new QVBoxLayout(&p_Wgt);
	p_Label = new QLabel("Найти");
    p_lEdit = new QLineEdit();
	p_listView = new QListView(NULL);

	p_vBox->addWidget(p_Label);
	p_vBox->addWidget(p_lEdit);
	p_vBox->addWidget(p_listView);

    p_listView->setModel(&modelList);
        setWidget(&p_Wgt);
    connect(p_lEdit, SIGNAL(textEdited(const QString &)), static_cast<wndMain *>(parent), SLOT(filterModel(const QString &)));
    connect(p_listView, SIGNAL(clicked(const QModelIndex &)), static_cast<wndMain *>(parent), SLOT(slotReadDB(const QModelIndex &)));
}

void wgtIndexDock::refreshModel() 
{
	if (db->isOpen()) {
        SQLEXEC_DATA sqlRes1 = Utils::sqlExecute(db, "SELECT id, title FROM data_tbl ORDER BY title");
		SQLEXEC_TABLE::iterator resIterator1 = sqlRes1.table.begin();
                QList <QPair<int, QString> > lstRows;
		QPair<int, QString> pairVal;
		for (; resIterator1 != sqlRes1.table.end(); ++resIterator1) {
            pairVal.second = (*resIterator1)["title"];
			pairVal.first = (*resIterator1)["id"].toInt();
			lstRows.push_back(pairVal);
		}
        modelList.setList(lstRows);
        proxyModel.setSourceModel(&modelList);
		proxyModel.setFilterCaseSensitivity(Qt::CaseInsensitive);
		p_listView->setModel(&proxyModel);
	}
}

int wgtIndexDock::getIdFromIndex(const QModelIndex &index)
{
    return modelList.getId(index);
}
void wgtIndexDock::setFilter(const QString &filter_str) {
    proxyModel.setFilterWildcard("*" + filter_str + "*");
}
