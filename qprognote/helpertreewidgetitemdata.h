#ifndef HELPERTREEWIDGETITEMDATA_H
#define HELPERTREEWIDGETITEMDATA_H

#include "helpertreewidgetitem.h"

class HelperTreeWidgetItemData : public HelperTreeWidgetItem
{
public:
    HelperTreeWidgetItemData(QTreeWidget *tree);
    HelperTreeWidgetItemData(QTreeWidgetItem *tree = NULL);
    ~HelperTreeWidgetItemData();
};

#endif // HELPERTREEWIDGETITEMDATA_H
