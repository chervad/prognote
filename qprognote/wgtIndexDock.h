#ifndef WGTINDEXDOCK_H
#define WGTINDEXDOCK_H

#include <QtGui>
#include <QtSql>
#include <QDockWidget>
#include <QVBoxLayout>
#include <QLabel>
#include "dataListModel.h"

class wgtIndexDock : public QDockWidget
{
public:
        wgtIndexDock(const QString &title, QWidget *parent = 0, Qt::WindowFlags flags = 0);
	virtual ~wgtIndexDock() {}
private:
	QSqlDatabase *db;
protected:
    dataListModel modelList;
	QSortFilterProxyModel proxyModel;
        QWidget p_Wgt;
	QVBoxLayout *p_vBox;
	QLabel *p_Label;
	QLineEdit *p_lEdit;
	QListView *p_listView;
public:
	void refreshModel();
	int getIdFromIndex(const QModelIndex &index);
    void setFilter(const QString &filter_str);
};

#endif // WGTINDEXDOCK_H
