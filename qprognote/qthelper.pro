# -------------------------------------------------
# Project created by QtCreator 2009-07-26T19:40:40
# -------------------------------------------------
QT += widgets sql 
TARGET = qthelper
TEMPLATE = app
SOURCES += main.cpp \
    wndmain.cpp \
    exsyntaxhighlighter.cpp \
    helpertreewidget.cpp \
    helpertreewidgetitem.cpp \
    helpertreewidgetitemdata.cpp \
    utils.cpp \
    helpertextedit.cpp \
    wgtIndexDock.cpp \
    dataListModel.cpp
HEADERS += wndmain.h \
    exsyntaxhighlighter.h \
    helpertreewidget.h \
    helpertreewidgetitem.h \
    helpertreewidgetitemdata.h \
    utils.h \
    helpertextedit.h \
    wgtIndexDock.h \
    dataListModel.h
RESOURCES += qthelper.qrc

#include(QtDotNetStyle/qtdotnetstyle.pri)
