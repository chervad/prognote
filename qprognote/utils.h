#ifndef UTILS_H
#define UTILS_H

#include <QtSql>
#include <QList>
#include <QMap>

typedef QList <QMap <QString, QString> > SQLEXEC_TABLE;

//#define RUS(txt) QString::fromLocal8Bit(txt)

typedef struct {
    SQLEXEC_TABLE table;
    QList <QString > sqlFields;
	qlonglong lastId;
} SQLEXEC_DATA;

class Utils
{
public:
    Utils();
	static bool isError() { return error; }
    static SQLEXEC_DATA sqlExecute(QSqlDatabase *db, QString sQuery, ...);
	static SQLEXEC_DATA sqlExecuteQuery(QSqlDatabase *db, QString sQuery);
	static void showError(QString message, bool add2Debug = true, QWidget *parent = NULL);
private:
	static bool error;
};

#endif // UTILS_H
