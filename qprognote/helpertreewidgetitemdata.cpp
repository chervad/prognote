#include <QtGui>
#include "helpertreewidgetitemdata.h"
#include "utils.h"

class Utils;

HelperTreeWidgetItemData::HelperTreeWidgetItemData(QTreeWidget *tree /* = NULL */)
        : HelperTreeWidgetItem(tree)
{
    nTag = 0;
    nType = HelperTreeWidgetItem::DATA_TYPE;
    setIcon(0, QPixmap(":/res/img/img-item.png"));
}

HelperTreeWidgetItemData::HelperTreeWidgetItemData(QTreeWidgetItem *tree /* = NULL */)
: HelperTreeWidgetItem(tree)
{
    nTag = 0;
    nType = HelperTreeWidgetItem::DATA_TYPE;
    setIcon(0, QPixmap(":/res/img/img-item.png"));
}

HelperTreeWidgetItemData::~HelperTreeWidgetItemData() {
}