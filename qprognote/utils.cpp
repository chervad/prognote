#include "utils.h"
#include <QErrorMessage>
#include <QtGui>

bool Utils::error = false;

Utils::Utils()
{
}

SQLEXEC_DATA Utils::sqlExecute(QSqlDatabase *db, QString sQuery, ...) {
    SQLEXEC_DATA res;
    QMap <QString, QString> tmpMap;
	res.lastId = 0;

    if (!db) {
        qDebug() << "Wrong pointer QSqlDatabase *db";
    }
    else if (!db->isOpen()) {
        qDebug() << "Database not open.";
    }
    else {
        va_list ap;
        va_start(ap, sQuery);
        sQuery.vsprintf(sQuery.toStdString().c_str(), ap);
        va_end(ap);

        QSqlQuery query(*db);
        QSqlRecord rec;
#ifdef _DEBUG
		qDebug() << "SQL Execute: " << sQuery;
#endif
        if (query.exec(sQuery)) {
            rec = query.record();
            res.sqlFields.clear();
            for (int a = 0; a < rec.count(); a++) {
                res.sqlFields.append(rec.fieldName(a));
            }

            while (query.next()) {
                tmpMap.clear();
                for (int a = 0; a < rec.count(); a++) {
                    tmpMap[rec.fieldName(a)] = query.value(a).toString();
                }
                res.table.append(tmpMap);
            }
			error = false;
			res.lastId = query.lastInsertId().toLongLong();
        }
        else {
            qDebug() << "Cannot execute SQL: " << sQuery << endl << "Error: " << query.lastError();
			error = true;
        }
    }
    return res;
}

SQLEXEC_DATA Utils::sqlExecuteQuery(QSqlDatabase *db, QString sQuery) {
	SQLEXEC_DATA res;
	QMap <QString, QString> tmpMap;
	res.lastId = 0;

	if (!db) {
		qDebug() << "Wrong pointer QSqlDatabase *db";
	}
	else if (!db->isOpen()) {
		qDebug() << "Database not open.";
	}
	else {
		QSqlQuery query(*db);
		QSqlRecord rec;
#ifdef _DEBUG
		qDebug() << "SQL Execute: " << sQuery;
#endif
		if (query.exec(sQuery)) {
			rec = query.record();
			res.sqlFields.clear();
			for (int a = 0; a < rec.count(); a++) {
				res.sqlFields.append(rec.fieldName(a));
			}

			while (query.next()) {
				tmpMap.clear();
				for (int a = 0; a < rec.count(); a++) {
					tmpMap[rec.fieldName(a)] = query.value(a).toString();
				}
				res.table.append(tmpMap);
			}
			error = false;
			res.lastId = query.lastInsertId().toLongLong();
		}
		else {
			qDebug() << "Cannot execute SQL: " << sQuery << endl << "Error: " << query.lastError();
			error = true;
		}
	}
	return res;
}

void Utils::showError(QString message, bool add2Debug /* = true*/, QWidget *parent /* = NULL */) {
	if (add2Debug) {
		qDebug() << message;
	}
	(new QErrorMessage(parent))->showMessage(message);
}
