#include "helpertreewidget.h"
#include "helpertreewidgetitem.h"
#include "utils.h"

HelperTreeWidgetItem::HelperTreeWidgetItem(QTreeWidget *tree /* = NULL */)
        : QTreeWidgetItem(tree)
{
    nTag = 0;
    nType = HelperTreeWidgetItem::TREE_TYPE;
    setIcon(0, QPixmap(":/res/img/img-folder.png"));
	setFlags(flags() | Qt::ItemIsEditable);
}

HelperTreeWidgetItem::HelperTreeWidgetItem(QTreeWidgetItem *tree /* = NULL */)
: QTreeWidgetItem(tree)
{
    nTag = 0;
    nType = HelperTreeWidgetItem::TREE_TYPE;
    setIcon(0, QPixmap(":/res/img/img-folder.png"));
	setFlags(flags() | Qt::ItemIsEditable);
}

HelperTreeWidgetItem::~HelperTreeWidgetItem() {
	if (this->parent()) {
		this->parent()->setSelected(false);
	}
}

void HelperTreeWidgetItem::setTag(int _tag) {
    nTag = _tag;
}

int HelperTreeWidgetItem::tag() {
    return nTag;
}

int HelperTreeWidgetItem::type() {
    return nType;
}

void HelperTreeWidgetItem::setData(int column, int role, const QVariant &value) {
	QTreeWidgetItem::setData(column, role, value);
	if (column == 0 && value.toString() != "" && role == 2) {
			QSqlDatabase *db = static_cast<HelperTreeWidget *>(this->treeWidget())->getDB();

			QString title = value.toString().toUtf8();

			if (this->nType == TREE_TYPE) {
				Utils::sqlExecute(db, "UPDATE tree_tbl SET title = '%s' WHERE id = %d", title.toStdString().c_str(), nTag);
			}
			else if (this->nType == DATA_TYPE) {
				Utils::sqlExecute(db, "UPDATE data_tbl SET title = '%s' WHERE id = %d", title.toStdString().c_str(), nTag);
			}
	}
}