#ifndef HELPERTREEWIDGETITEM_H
#define HELPERTREEWIDGETITEM_H

#include <QTreeWidgetItem>

//#define RUS(txt) QString::fromLocal8Bit(txt)

class HelperTreeWidgetItem : public QTreeWidgetItem
{
public:
    enum {
       TREE_TYPE = 1,
       DATA_TYPE = 2
    };

    HelperTreeWidgetItem(QTreeWidget *tree);
    HelperTreeWidgetItem(QTreeWidgetItem *tree = NULL);
    ~HelperTreeWidgetItem();
	virtual void setData(int column, int role, const QVariant &value);
    void setTag(int _tag);
    int tag();
    int type();
protected:
    int nTag;
    int nType;
};

#endif // HELPERTREEWIDGETITEM_H
