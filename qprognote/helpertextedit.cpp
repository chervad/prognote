#include <QDebug>
#include "helpertextedit.h"
#include "wndmain.h"
#include "utils.h"

HelperTextEdit::HelperTextEdit(QWidget *parent /* = NULL */)
        :QPlainTextEdit(parent)
{
}

void HelperTextEdit::focusOutEvent(QFocusEvent *event) {
    qobject_cast<wndMain *>(parent())->saveCurentText();
}