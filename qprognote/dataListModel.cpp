#include "dataListModel.h"

dataListModel::dataListModel(QObject *parent /* =NULL */)
    : QAbstractListModel(parent)
    , m_lstData(QList <QPair<int, QString> >())
{

}

QVariant dataListModel::data(const QModelIndex &index, int role /* =Qt::DisplayRole */) const
{
	if (!index.isValid()) {
		return QVariant();
	}
    return (role == Qt::DisplayRole || role == Qt::EditRole) ? m_lstData.at(index.row()).second : QVariant();
}

int dataListModel::rowCount(const QModelIndex &parent /* =QModelIndex*/) const
{
    return m_lstData.size();
}

void dataListModel::setList(QList <QPair<int, QString> > &lstData)
{
    m_lstData.clear();
    m_lstData = lstData;
}

int dataListModel::getId(const QModelIndex &index) 
{
	if (!index.isValid() || index.row() + 1 > m_lstData.size()) {
        return QVariant().toInt();
	}
    return m_lstData.at(index.row()).first;
}
