#ifndef HELPERTEXTEDIT_H
#define HELPERTEXTEDIT_H

#include <QPlainTextEdit>

class HelperTextEdit : public QPlainTextEdit
{
    Q_OBJECT
public:
    HelperTextEdit(QWidget *paret = NULL);
protected:
    virtual void focusOutEvent(QFocusEvent *event);
};

#endif // HELPERTEXTEDIT_H
