#ifndef WNDMAIN_H
#define WNDMAIN_H

#include <QtGui>
#include <QtSql>
#include <QMainWindow>

#include "exsyntaxhighlighter.h"
#include "helpertreewidget.h"
#include "helpertextedit.h"
#include "wgtIndexDock.h"

class wndMain : public QMainWindow
{
    Q_OBJECT
public:
    wndMain(QWidget *parent = 0);
    ~wndMain();
public:
    QSqlDatabase *getDBPointer() { return &db; }
    void saveCurentText();
protected:
    void createActions();
    void createMenus();
    void createToolBar();
    void createStatusBar();    
    void createLeftPart();
    void createMainTextArea();

    bool openDataBase();
    bool closeDataBase();
private:
    QSqlDatabase db;
    QString file_db;
    bool b_opened_db;
    QMap <QString, QAction *> appActions;
    QMap <QString, QMenu *> appMainMenu;
    QMap <QString, QToolBar *> appToolBars;
    HelperTextEdit *txtMain;
    QDockWidget *p_dwParts;
    wgtIndexDock *p_dwIndexes;

    HelperTreeWidget *treeParts;
    ExSyntaxHighlighter *highlighter;
    unsigned long n_dataId;
    unsigned long n_treeType;
public:
    QMap <QString, QAction *> getAppActions() {return appActions; }
public slots:
    void slotReadDB(QTreeWidgetItem *item, int column);
    void slotReadDB(const QModelIndex &index);
    void slotOpenDataBase();
    void slotCloseDataBase();
    void slotChangeTreeMenu(bool);
    void slotChangeSubTreeMenu(bool);
    void slotDelPart();
    void slotRenamePart();
    void slotAddPart();
    void slotAddSubPart();
    void slotAddData();
    void filterModel(const QString &text);
};

#endif // WNDMAIN_H
