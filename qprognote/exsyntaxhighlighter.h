#ifndef EXSYNTAXHIGHLIGHTER_H
#define EXSYNTAXHIGHLIGHTER_H

#include <QSyntaxHighlighter>

#include <QHash>
#include <QTextCharFormat>

QT_BEGIN_NAMESPACE
class QTextDocument;
QT_END_NAMESPACE

class ExSyntaxHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    ExSyntaxHighlighter(QTextDocument *parent = 0);

protected:
    void highlightBlock(const QString &text);

private:
    struct ExHighlightingRule
    {
        QRegExp pattern;
        QTextCharFormat format;
    };
    QVector<ExHighlightingRule> exHighlightingRules;

    QRegExp commentStartExpression;
    QRegExp commentEndExpression;

    QTextCharFormat keywordFormat;
    QTextCharFormat directivesFormat;
    QTextCharFormat classFormat;
    QTextCharFormat singleLineCommentFormat;
    QTextCharFormat multiLineCommentFormat;
    QTextCharFormat quotationFormat;
    QTextCharFormat functionFormat;
};

#endif // EXSYNTAXHIGHLIGHTER_H
