﻿#include "wndmain.h"
#include "utils.h"

#include <QAction>
#include <QMenuBar>
#include <QToolBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QMessageBox>

class Utils;

wndMain::wndMain(QWidget *parent)
    : QMainWindow(parent)
{
    createActions();
    createMenus();
    createToolBar();
    createStatusBar();
    createLeftPart();
    createMainTextArea();

	file_db = "";
    n_treeType = 0;
    n_dataId = 0;
	b_opened_db = false;
}

wndMain::~wndMain() {
    delete highlighter;
    closeDataBase();
}

void wndMain::createActions() {
    QAction *tmpAction = NULL;

    tmpAction = new QAction(QIcon(":/res/img/img-browse.png"), "&Открыть базу данных...", this);
    tmpAction->setShortcut(Qt::CTRL + Qt::Key_O);
    tmpAction->setStatusTip("Открыть файл базы данных");
    appActions.insert("openDataBase", tmpAction);

    tmpAction = new QAction(QIcon(":/res/img/img-stop.png"), "&Закрыть базу данных", this);
    tmpAction->setShortcut(Qt::CTRL + Qt::Key_E);
    tmpAction->setStatusTip("Закрыть текущий файл базы данных");
    appActions.insert("closeDataBase", tmpAction);

    tmpAction = new QAction(QIcon(":/res/img/img-config.png"), "&Настройки...", this);
    tmpAction->setShortcut(Qt::ALT + Qt::Key_P);
    tmpAction->setStatusTip("Настроить программу");
    appActions.insert("prefApp", tmpAction);

    tmpAction = new QAction(QIcon(":/res/img/img-quit.png"), "&Выйти", this);
    tmpAction->setShortcut(Qt::CTRL + Qt::Key_Q);
    tmpAction->setStatusTip("Выйти из приложения");
    appActions.insert("exitApp", tmpAction);

    tmpAction = new QAction("Добавить &раздел", this);
    tmpAction->setShortcut(Qt::CTRL + Qt::Key_P);
    tmpAction->setStatusTip("Добавить новый раздел в текущую позицию");
    appActions.insert("addPart", tmpAction);

    tmpAction = new QAction("Добавить &подраздел", this);
    tmpAction->setShortcut(Qt::CTRL + Qt::Key_S);
    tmpAction->setStatusTip("Добавить новый подраздел в текущий раздел");
    appActions.insert("addSubPart", tmpAction);

	tmpAction = new QAction(QIcon(":/res/img/img-pen.png"), "Добавить &текст", this);
	tmpAction->setShortcut(Qt::CTRL + Qt::Key_T);
	tmpAction->setStatusTip("Добавить новый текстовый подраздел в текущий раздел");
	appActions.insert("addData", tmpAction);
    
    tmpAction = new QAction(QIcon(":/res/img/img-trash.png"), "&Удалить раздел / подраздел", this);
    tmpAction->setShortcut(Qt::Key_Delete);
    tmpAction->setStatusTip("Удалить текущий раздел / подраздел со всеми вложенями");
    appActions.insert("delPart", tmpAction);

    tmpAction = new QAction("Пере&именовать раздел / подраздел", this);
    tmpAction->setShortcut(Qt::Key_F2);
    tmpAction->setStatusTip("Переименовать текущий раздел / подраздел");
    appActions.insert("renamePart", tmpAction);

    tmpAction = new QAction("Пере&нести раздел / подраздел", this);
    tmpAction->setStatusTip("Перенести текущий раздел / подраздел на другой ровень");
    appActions.insert("transPart", tmpAction);

    tmpAction = new QAction("&Отмена", this);
    tmpAction->setShortcut(Qt::CTRL + Qt::Key_Z);
    tmpAction->setStatusTip("Отменить последню операцию редактирования текста");
    appActions.insert("undoEdit", tmpAction);

    tmpAction = new QAction("&Повтор", this);
    tmpAction->setShortcut(Qt::CTRL + Qt::Key_Y);
    tmpAction->setStatusTip("Повторить последню отменённую операцию редактирования текста");
    appActions.insert("redoEdit", tmpAction);

    tmpAction = new QAction("&Вырезать", this);
    tmpAction->setShortcut(Qt::CTRL + Qt::Key_X);
    tmpAction->setStatusTip("Удалить выделенный текст и поместить в буфер обмена");
    appActions.insert("putEdit", tmpAction);

    tmpAction = new QAction("&Копировать", this);
    tmpAction->setShortcut(Qt::CTRL + Qt::Key_C);
    tmpAction->setStatusTip("Копировать выделенный текст в буфер обмена");
    appActions.insert("copyEdit", tmpAction);

    tmpAction = new QAction("В&ставить", this);
    tmpAction->setShortcut(Qt::CTRL + Qt::Key_V);
    tmpAction->setStatusTip("Вставить в текущую позицию текст из буфера обмена");
    appActions.insert("pasteEdit", tmpAction);

    tmpAction = new QAction("&Удалить", this);
    tmpAction->setShortcut(Qt::Key_Delete);
    tmpAction->setStatusTip("Удалить выделенный текст");
    appActions.insert("delEdit", tmpAction);

    tmpAction = new QAction("Удалить вс&ё", this);
    tmpAction->setStatusTip("Удалить весь текст");
    appActions.insert("delAllEdit", tmpAction);

    tmpAction = new QAction(QIcon(":/res/img/img-info.png"), "&О программе...", this);
    tmpAction->setStatusTip("О программе");
    appActions.insert("helpAbout", tmpAction);

    tmpAction = new QAction(QIcon(":/res/img/img-help.png"), "&Справка...", this);
    tmpAction->setStatusTip("Вызов справки");
    appActions.insert("helpContextHelp", tmpAction);

    tmpAction = new QAction("&Поддержка", this);
    tmpAction->setStatusTip("Открыть веб-узел тех. поддержки");
    appActions.insert("helpSupport", tmpAction);

    connect(appActions["exitApp"], SIGNAL(triggered()), this, SLOT(close()));
    connect(appActions["openDataBase"], SIGNAL(triggered()), this, SLOT(slotOpenDataBase()));
	connect(appActions["closeDataBase"], SIGNAL(triggered()), this, SLOT(slotCloseDataBase()));

	connect(appActions["addPart"], SIGNAL(triggered()), this, SLOT(slotAddPart()));
	connect(appActions["addSubPart"], SIGNAL(triggered()), this, SLOT(slotAddSubPart()));
	connect(appActions["delPart"], SIGNAL(triggered()), this, SLOT(slotDelPart()));
	connect(appActions["renamePart"], SIGNAL(triggered()), this, SLOT(slotRenamePart()));
	connect(appActions["addData"], SIGNAL(triggered()), this, SLOT(slotAddData()));
}

void wndMain::createMenus() {
    appMainMenu.insert("menuFile", menuBar()->addMenu("&Файл"));
    appMainMenu.insert("menuTreeParts", menuBar()->addMenu("&Дерево разделов"));
    appMainMenu.insert("menuEdit", menuBar()->addMenu("П&равка"));
    appMainMenu.insert("menuHelp", menuBar()->addMenu("&Помощь"));

    appMainMenu["menuFile"]->addAction(appActions["openDataBase"]);
    appMainMenu["menuFile"]->addAction(appActions["closeDataBase"]);
    appMainMenu["menuFile"]->addSeparator();
    appMainMenu["menuFile"]->addAction(appActions["prefApp"]);
    appMainMenu["menuFile"]->addSeparator();
    appMainMenu["menuFile"]->addAction(appActions["exitApp"]);

    appMainMenu["menuTreeParts"]->addAction(appActions["addPart"]);
    appMainMenu["menuTreeParts"]->addAction(appActions["addSubPart"]);
	appMainMenu["menuTreeParts"]->addAction(appActions["addData"]);
    appMainMenu["menuTreeParts"]->addSeparator();
    appMainMenu["menuTreeParts"]->addAction(appActions["delPart"]);
    appMainMenu["menuTreeParts"]->addAction(appActions["renamePart"]);
    appMainMenu["menuTreeParts"]->addSeparator();
    appMainMenu["menuTreeParts"]->addAction(appActions["transPart"]);

	appActions["addPart"]->setEnabled(false);
	appActions["addSubPart"]->setEnabled(false);
	appActions["addData"]->setEnabled(false);
	appActions["delPart"]->setEnabled(false);
	appActions["renamePart"]->setEnabled(false);
	appActions["transPart"]->setEnabled(false);

    appMainMenu["menuEdit"]->addAction(appActions["undoEdit"]);
    appMainMenu["menuEdit"]->addAction(appActions["redoEdit"]);
    appMainMenu["menuEdit"]->addSeparator();
    appMainMenu["menuEdit"]->addAction(appActions["putEdit"]);
    appMainMenu["menuEdit"]->addAction(appActions["copyEdit"]);
    appMainMenu["menuEdit"]->addAction(appActions["pasteEdit"]);
    appMainMenu["menuEdit"]->addAction(appActions["delEdit"]);
    appMainMenu["menuEdit"]->addSeparator();
    appMainMenu["menuEdit"]->addAction(appActions["delAllEdit"]);

    appMainMenu["menuHelp"]->addAction(appActions["helpAbout"]);
    appMainMenu["menuHelp"]->addAction(appActions["helpContextHelp"]);
    appMainMenu["menuHelp"]->addSeparator();
    appMainMenu["menuHelp"]->addAction(appActions["helpSupport"]);

    appActions["closeDataBase"]->setEnabled(false);
    appMainMenu["menuTreeParts"]->setEnabled(false);
	appActions["delPart"]->setEnabled(false);
	appActions["renamePart"]->setEnabled(false);
    appMainMenu["menuEdit"]->setEnabled(false);
	appActions["undoEdit"]->setEnabled(false);
	appActions["redoEdit"]->setEnabled(false);
	appActions["putEdit"]->setEnabled(false);
	appActions["copyEdit"]->setEnabled(false);
	appActions["pasteEdit"]->setEnabled(false);
	appActions["delEdit"]->setEnabled(false);
	appActions["delAllEdit"]->setEnabled(false);
}

void wndMain::createToolBar() {
    appToolBars.insert("fileActions", addToolBar("fileActions"));
	appToolBars["fileActions"]->addAction(appActions["openDataBase"]);
	appToolBars["fileActions"]->addAction(appActions["closeDataBase"]);

	appToolBars.insert("dbActions", addToolBar("dbActions"));
	appToolBars["dbActions"]->addAction(appActions["addData"]);
	appToolBars["dbActions"]->addAction(appActions["delPart"]);
}

void wndMain::createStatusBar() {
    statusBar()->showMessage("Готово");
}

void wndMain::createLeftPart() {
    p_dwParts = new QDockWidget("Разделы", this);
	p_dwIndexes = new wgtIndexDock("Указатель", this);

	p_dwParts->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);

	treeParts = new HelperTreeWidget(p_dwParts);
	treeParts->setHeaderHidden(true);
	treeParts->setDragEnabled(true);
	treeParts->setAcceptDrops(true);

	p_dwParts->setWidget(treeParts);

	setTabPosition(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea, QTabWidget::North);
	addDockWidget(Qt::LeftDockWidgetArea, p_dwParts);
	addDockWidget(Qt::LeftDockWidgetArea, (QDockWidget *)p_dwIndexes);
	tabifyDockWidget(p_dwParts, (QDockWidget *)p_dwIndexes); 	

	p_dwParts->setVisible(false);
	p_dwIndexes->setVisible(false);

	connect(p_dwParts, SIGNAL(visibilityChanged(bool)), this, SLOT(slotChangeTreeMenu(bool)));
    connect(treeParts, SIGNAL(itemClicked(QTreeWidgetItem *, int)), this, SLOT(slotReadDB(QTreeWidgetItem *, int)));
	connect(treeParts, SIGNAL(signalFocus(bool)), this, SLOT(slotChangeSubTreeMenu(bool)));
}

void wndMain::createMainTextArea() {
    txtMain = new HelperTextEdit(this);
    setCentralWidget(txtMain);
    txtMain->setVisible(false);

    highlighter = new ExSyntaxHighlighter(txtMain->document());
}

bool wndMain::openDataBase() {
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(file_db);

    db.setUserName("root");
    db.setHostName("localhost");
    db.setPassword("root");

    if (!db.open()) {
		Utils::showError(QString("%1: %2").arg("Невозможно открыть базу данных").arg(db.lastError().text()), this);
        return false;
    }
    else {
		qDebug() << "База данных открыта успешно";
    }
	treeParts->setDB(&db);
	return true;
}

bool wndMain::closeDataBase() {
	if (db.open()) {
		if (db.commit()) {
			qDebug() << "БД зафиксирована";
		}
		db.close();
	}
	return false;
}

void wndMain::saveCurentText() {
    if (db.open()) {
        if (n_dataId > 0 && n_treeType == 2) {
            QString text = txtMain->document()->toHtml().replace("'", "").toUtf8();
            SQLEXEC_DATA sqlRes = Utils::sqlExecute(&db, "UPDATE data_tbl SET data = '%s' WHERE id = %d", text.toStdString().c_str(), n_dataId);
        }
    }
} 

/************************************************************************/
/*								SLOTS                                   */
/************************************************************************/

void wndMain::slotReadDB(QTreeWidgetItem * item, int column) {
	QSqlQuery query(db);
	QSqlRecord rec;

	SQLEXEC_DATA sqlRes;

	n_treeType = static_cast<HelperTreeWidgetItem *>(item)->type();
	n_dataId = static_cast<HelperTreeWidgetItem *>(item)->tag();
	if (n_treeType == 2 && n_dataId > 0) {
		sqlRes = Utils::sqlExecute(&db, "SELECT data FROM data_tbl WHERE id = %d", n_dataId);
		if (sqlRes.table.count())  {
			txtMain->document()->setHtml(sqlRes.table.at(0)["data"]);
			txtMain->setEnabled(true);
		}
	}
	else {
		txtMain->document()->clear();
		txtMain->setEnabled(false);
	}
	if (n_treeType == HelperTreeWidgetItem::TREE_TYPE) {
		appActions["addPart"]->setEnabled(true);
		appActions["addSubPart"]->setEnabled(true);
		appActions["addData"]->setEnabled(true);
	}
	else {
		appActions["addPart"]->setEnabled(false);
		appActions["addSubPart"]->setEnabled(false);
		appActions["addData"]->setEnabled(true);
	}
	slotChangeSubTreeMenu(true);
}

void wndMain::slotReadDB(const QModelIndex &index)
{
	treeParts->clearFocus();
	n_dataId = p_dwIndexes->getIdFromIndex(index);
	SQLEXEC_DATA sqlRes = Utils::sqlExecute(&db, "SELECT data FROM data_tbl WHERE id = %d", n_dataId);
	if (sqlRes.table.count())  {
		txtMain->document()->setHtml(sqlRes.table.at(0)["data"]);
		txtMain->setEnabled(true);
	}
	else {
		txtMain->document()->clear();
		txtMain->setEnabled(false);
	}
}

void wndMain::slotOpenDataBase() {
	QString temp_file_db = "";
	if ((temp_file_db = QFileDialog::getOpenFileName(this, "Открыть файл базы данных", "", "*.dat")) != "")
	{
		if (temp_file_db == file_db) {
			Utils::showError("Файл базы данных уже открыт");
			return;
		}
		if (b_opened_db) {
			if (QMessageBox::question(this, "Закрыть текущий файл БД", "Закрыть текущий файл базы данных?", "Да", "Нет") == 1) {
			    return;
			}
			b_opened_db = closeDataBase();
		}
		file_db = temp_file_db;

		b_opened_db = openDataBase();

		appActions["closeDataBase"]->setEnabled(true);
		slotChangeTreeMenu(true);
		appMainMenu["menuEdit"]->setEnabled(true);

		treeParts->refreshLeftTree();
		treeParts->expandAll();
		p_dwIndexes->refreshModel();
		txtMain->clear();
		txtMain->setVisible(true);
		p_dwParts->setVisible(true);
		p_dwIndexes->setVisible(true);
		appActions["addSubPart"]->setEnabled(false);
		appActions["addData"]->setEnabled(false);
		appActions["addPart"]->setEnabled(true);
	}
}

void wndMain::slotCloseDataBase() {
	if (b_opened_db) {
		appActions["closeDataBase"]->setEnabled(false);
		slotChangeTreeMenu(false);
		appMainMenu["menuEdit"]->setEnabled(false);

		appActions["addPart"]->setEnabled(false);
		appActions["addSubPart"]->setEnabled(false);
		appActions["addData"]->setEnabled(false);
		appActions["delPart"]->setEnabled(false);
		appActions["renamePart"]->setEnabled(false);
		appActions["transPart"]->setEnabled(false);

		txtMain->setVisible(false);
		p_dwParts->setVisible(false);
		p_dwIndexes->setVisible(false);

		b_opened_db = closeDataBase();
		file_db = "";
	}
}

void wndMain::slotChangeTreeMenu(bool status) {
	if (b_opened_db) {
		slotChangeSubTreeMenu(false);
		appMainMenu["menuTreeParts"]->setEnabled(status);
	}
}

void wndMain::slotChangeSubTreeMenu(bool status) {
	appActions["delPart"]->setEnabled(status);
	appActions["renamePart"]->setEnabled(status);
}

void wndMain::slotDelPart() {
	QList<QTreeWidgetItem *> treeItemList = treeParts->selectedItems();
	HelperTreeWidgetItem *ptwgItem = NULL, *ptwgSelItem = NULL;

	if (treeItemList.count() > 0) {
		ptwgSelItem = static_cast <HelperTreeWidgetItem *>(treeItemList.at(0));
		QList<QTreeWidgetItem *>::iterator listIterator = treeItemList.begin();
		for (;listIterator != treeItemList.end(); ++listIterator) {
			ptwgItem = static_cast <HelperTreeWidgetItem *>(*listIterator);
			if (ptwgItem->type() == HelperTreeWidgetItem::TREE_TYPE) {
				SQLEXEC_DATA sqlRes0 = Utils::sqlExecute(&db, "SELECT count() as count FROM tree_tbl WHERE parent = %d", ptwgItem->tag());
				SQLEXEC_DATA sqlRes1 = Utils::sqlExecute(&db, "SELECT count() as count FROM data_tbl WHERE tree_id = %d", ptwgItem->tag());

				if (sqlRes0.table.at(0)["count"].toInt() > 0 || sqlRes1.table.at(0)["count"].toInt()) {
					int answer = QMessageBox::question(this, "Удаление раздела", 
						"Вы хотите удалить текущий раздел со всеми подразделами?", 
						"Да", "Нет", QString(), 0, 1);
					if (answer == 0) {
						// to-do удаление подраздела рекурсивно
						treeParts->deleteTreeRecursive(ptwgItem->tag());
						delete ptwgSelItem;
						appActions["addSubPart"]->setEnabled(false);
					}
				}
				else {
					Utils::sqlExecute(&db, "DELETE FROM tree_tbl WHERE id = %d", ptwgItem->tag());
					delete ptwgSelItem;
					appActions["addSubPart"]->setEnabled(false);
				}
			}
			else if (ptwgItem->type() == HelperTreeWidgetItem::DATA_TYPE) {
				Utils::sqlExecute(&db, "DELETE FROM data_tbl WHERE id = %d", ptwgItem->tag());
				delete ptwgSelItem;
				appActions["addSubPart"]->setEnabled(false);
			}
		}
	} 
}

void wndMain::slotRenamePart() {
	QList<QTreeWidgetItem *> treeItemList = treeParts->selectedItems();
	HelperTreeWidgetItem *ptwgItem = NULL;

	if (treeItemList.count() == 1) {
		ptwgItem = static_cast <HelperTreeWidgetItem *>(treeItemList.at(0));
		treeParts->editItem(ptwgItem, 0);
	}
}

void wndMain::slotAddPart() {
	QList<QTreeWidgetItem *> treeItemList = treeParts->selectedItems();
	HelperTreeWidgetItem *ptwgItem = NULL;
	HelperTreeWidgetItem *ptwgNewItem = NULL;
	SQLEXEC_DATA sqlRes;
	int parent = 0;
	if (treeItemList.count() > 0) {
		ptwgItem = static_cast <HelperTreeWidgetItem *>(treeItemList.at(0));
		ptwgItem->setSelected(false);
		if (ptwgItem->type() == HelperTreeWidgetItem::TREE_TYPE) {
			sqlRes = Utils::sqlExecute(&db, "SELECT parent as parent FROM tree_tbl WHERE id = %d", ptwgItem->tag());
		}
		else if (ptwgItem->type() == HelperTreeWidgetItem::DATA_TYPE) {
			sqlRes = Utils::sqlExecute(&db, "SELECT tree_id as parent FROM data_tbl WHERE id = %d", ptwgItem->tag());
		}
		parent = sqlRes.table.at(0)["parent"].toInt();
		sqlRes = Utils::sqlExecute(&db, "INSERT INTO tree_tbl('id', 'parent', 'title') VALUES (NULL, %d, 'New part') ", parent);
		if (ptwgItem->parent()) {
			ptwgNewItem = new HelperTreeWidgetItem(ptwgItem->parent());
		}
		else {
			ptwgNewItem = new HelperTreeWidgetItem(treeParts);
		}
	}
	else {
		sqlRes = Utils::sqlExecute(&db, "INSERT INTO tree_tbl('id', 'parent', 'title') VALUES (NULL, 0, 'New part') ");
		ptwgNewItem = new HelperTreeWidgetItem(treeParts);
	}
	ptwgNewItem->setTag(sqlRes.lastId);
	ptwgNewItem->setText(0, "New part");
	ptwgNewItem->setSelected(true);
	appActions["addSubPart"]->setEnabled(true);
	treeParts->editItem(ptwgNewItem, 0);
}

void wndMain::slotAddSubPart() {
	QList<QTreeWidgetItem *> treeItemList = treeParts->selectedItems();
	HelperTreeWidgetItem *ptwgItem = NULL;
	HelperTreeWidgetItem *ptwgNewItem = NULL;
	SQLEXEC_DATA sqlRes;
	int parent = 0;
	if (treeItemList.count() > 0) {
		ptwgItem = static_cast <HelperTreeWidgetItem *>(treeItemList.at(0));
		if (ptwgItem->type() == HelperTreeWidgetItem::DATA_TYPE) {
			/*int answer = */QMessageBox::information(this, "Добавление подраздела", "Дабавить подраздел в данную позицию нельзя!", "Закрыть");
			return;
		}

		ptwgItem->setSelected(false);
		parent = ptwgItem->tag();
		sqlRes = Utils::sqlExecute(&db, "INSERT INTO tree_tbl('id', 'parent', 'title') VALUES (NULL, %d, 'New part') ", parent);
		ptwgNewItem = new HelperTreeWidgetItem(ptwgItem);
		ptwgItem->setExpanded(true);
		ptwgNewItem->setTag(sqlRes.lastId);
		ptwgNewItem->setText(0, "New part");
		ptwgNewItem->setSelected(true);
		appActions["addSubPart"]->setEnabled(true);
		treeParts->editItem(ptwgNewItem, 0);
	}
}

void wndMain::slotAddData() {
	QList<QTreeWidgetItem *> treeItemList = treeParts->selectedItems();
	HelperTreeWidgetItem *ptwgItem = NULL;
	HelperTreeWidgetItem *ptwgNewItem = NULL;
	SQLEXEC_DATA sqlRes;
	int parent = 0;

	if (treeItemList.count() > 0) {
		ptwgItem = static_cast <HelperTreeWidgetItem *>(treeItemList.at(0));
		if (ptwgItem->type() == HelperTreeWidgetItem::DATA_TYPE) {
			/*int answer = */QMessageBox::information(this, "Добавление данных", "Дабавить данные в данную позицию нельзя!", "Закрыть");
			return;
		}

		ptwgItem->setSelected(false);
		parent = ptwgItem->tag();
		sqlRes = Utils::sqlExecute(&db, "INSERT INTO data_tbl('id', 'tree_id', 'title', 'data') VALUES (NULL, %d, 'New data', '') ", parent);
		ptwgNewItem = new HelperTreeWidgetItemData(ptwgItem);
		ptwgItem->setExpanded(true);
		ptwgNewItem->setTag(sqlRes.lastId);
		ptwgNewItem->setText(0, "New part");
		ptwgNewItem->setSelected(true);
		appActions["addSubPart"]->setEnabled(false);
		slotReadDB(ptwgNewItem, 0);
		treeParts->editItem(ptwgNewItem, 0);
	}
}

void wndMain::filterModel(const QString &text)
{
    p_dwIndexes->setFilter(text);
}
