@rem set QTDIR=C:\Qt\5.13.1\5.13.1\msvc2017
set APP_ROOT=%CD%
set BUILD_PATH=%APP_ROOT%\_build
set SRC_PATH=%APP_ROOT%\qprognote
set BUILD_TYPE=Release

@rem rmdir %BUILD_PATH% /q /s
mkdir %BUILD_PATH%
cd %BUILD_PATH%
cmake -G "Visual Studio 15" -DCMAKE_PREFIX_PATH=%QTDIR%\5.13.1\msvc2017 %SRC_PATH%
cmake --build . --target qprognote --config %BUILD_TYPE%
@rem pause